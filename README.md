# WhyYe?
- [Live](https://why-ye.netlify.com)

You ever asked yourself that question?
Fetch a random quote from Kanye West. 

Powered by [kanye.rest](kanye.rest)

## Project Screen Shot(s)

![Project Mockup](https://github.com/kjd3v/react-kanye/blob/master/mockups/full_mockup.jpg "Mockup")

## Installation and Setup Instructions
Clone down this repository. You will need node and npm installed globally on your machine.

### Installation:

`npm install`

### To Run Test Suite:

`npm test`

### To Start Server:

`npm start`

### To Visit App:

`localhost:3000/`


#### Features
- fetch jokes from kenyre.rest API

#### Tools
- React (w/ Hooks)
- Axios
- Styled-Components
- JSX, CSS, JS
