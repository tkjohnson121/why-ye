import React, { useState } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import axios from 'axios';
import { theme } from '../../theme';
import { TextStyled, FlexContainerStyled, ButtonStyled, LayerStyled } from '../../theme';

const AppBG = styled.div`
  height: 100vh;
  width: 100vw;
  background: linear-gradient(45.34deg, #EA52F8 5.66%, #0066FF 94.35%);
`;

function App() {
  const [quote, setQuote] = useState({ text: null, author: null });

  async function fetchQuote() {
    const res = await axios.get('https://api.kanye.rest');
    setQuote({ text: res.data.quote, author: "Kanye West" });
  };

  return (
    <>
      <ThemeProvider theme={theme}>
        <AppBG>
          <TextStyled
            align="center"
            color="primary"
            fontWeight="bold"
            as="p"
            padding="3rem 0"
            size="5rem"
          >
            Kanye Rest
          </TextStyled>
          <FlexContainerStyled>
            <LayerStyled
              elevation="float"
              margin="10%"
              padding="3rem 3rem 5rem"
            >
              <FlexContainerStyled flush>
                <ButtonStyled primary onClick={fetchQuote}>
                  Fetch New Quote
                </ButtonStyled>
              </FlexContainerStyled>
              <FlexContainerStyled column>
                <TextStyled
                  as="h1"
                  fontWeight="bold"
                  size="2em"
                  align="center"
                >
                  {quote.text || "Click That ☝️Button"}
                </TextStyled>
                <TextStyled
                  margin="1rem 0 0"
                  fontWeight="light"
                  size="1em"
                  align="center"
                >
                  {quote.author ? `by: ${quote.author}` : null}
                </TextStyled>
              </FlexContainerStyled>
            </LayerStyled>
          </FlexContainerStyled>
        </AppBG>
      </ThemeProvider>
    </>
  )
}

export default App
