import styled from 'styled-components';

export const theme = {
  colors: {
    primary: '#72e1d1',
    light: '#FAFAFF',
    dark: '#1C1C1C',
    danger: '#f5868b',
    info: "#f5868b"
  },

  elevations: {
    flat: "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);",
    high: "0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);",
    higher: "0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);",
    highest: "0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);",
    float: "0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);",
  },

  transitions: {
    pop: 'all 0.3s cubic-bezier(.25,.8,.25,1)',
  },

  fontFamily: "'Lato', 'Source Sans Pro', sans-serif"
}

export const TextStyled = styled.span`
  font-family: ${props => props.fontFamily || props.theme.fontFamily};
  font-weight: ${props => props.fontWeight || '400'};
  font-size: ${props => props.size};
  font-style: ${props => props.fontStyle || 'none'};
  text-align: ${props => props.align || 'left'};
  text-transform: ${props => props.transform || 'none'};
  text-decoration: ${props => props.decoration || 'none'};
  line-height: ${props => props.lineHeight || 'inherit'};
  margin: ${props => props.margin || 0};
  padding: ${props => props.padding || 0};
  color: ${props => props.theme.colors[props.color] || props.theme.colors.dark};
`;

export const ButtonStyled = styled.button`
  transform: scale(.9) translateY(3px);
  text-decoration: none;
  padding: .5rem 1rem;
  font-size: 1em;
  cursor: pointer;
  border: 2px solid;
  border-radius: 10px;
  margin:${props => props.flush ? 0 : '1rem 1rem'};
  box-shadow: ${props => props.elevated ? props.theme.elevations.flat : 'none'};
  transition: ${props => props.theme.transitions.pop};

  background-color:${
  props => props.primary ?
    props.theme.colors.primary :
    props.theme.colors.light
  };
  border-color:${
  props => props.primary ?
    props.theme.colors.primary :
    props.theme.colors.primary
  };
  color:${
  props => props.primary ?
    props.theme.colors.light :
    props.theme.colors.primary
  };

  &:hover,
  &:focus {
    transform: scale(1) translateY(0);
    box-shadow: ${props => props.theme.elevations.higher};
  }
`;

export const PageHeader = styled.h2`
  font-size: 3em;
  font-weight: 700;
`;

export const FlexContainerStyled = styled.div`
  width: ${props => props.width || '100%'};
  max-width: ${props => props.flush ? 'auto' : '1200px'};
  margin: 0 auto;
  padding-top: ${props => props.flush ? '0' : '3%'};
  padding-bottom: ${props => props.flush ? '0' : '3%'};

  display: flex;
  flex-wrap: ${props => props.wrap ? 'wrap' : 'no-wrap'};
  flex-direction: ${props => props.column ? 'column' : 'row'};
  justify-content: ${props => props.justify || 'center'};
  align-items: ${(props) => props.align || 'center'};

  @media only screen and (max-width: ${props => props.mediaQ || null}) {
    flex-direction: column;
    margin: ${props => props.margin || '1rem auto'};
  }
`;

export const LayerStyled = styled.div`
  background-color: ${props => props.theme.colors.light};
  border-radius: 10px;
  width: ${props => props.width || '100%'};
  height: ${props => props.height || 'auto'};
  border: ${props => props.border || 'none'};
  padding: ${props => props.padding ? props.padding : '3%'};
  margin: ${props => props.margin ? props.margin : '3rem auto'};
  box-shadow: ${props => props.theme.elevations[props.elevation] || 'none'};
`;

export const ImageStyled = styled.img`
  width: ${props => props.width || '100%'};
  max-width: 100%;
  max-width: 600px;
  min-width: 200px;
  border-radius: 10px;
  margin: ${props => props.margin || '1rem auto'};
  box-shadow: ${props => props.elevated ? props.theme.elevations.high : 'none'};
  transition: ${props => props.theme.transitions.pop};
`;
