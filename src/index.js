import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/App';
import './assets/reset.css';

ReactDOM.render(<App />, document.querySelector('#root'))
